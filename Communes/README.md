# Exercice

## SOURCE :: Xavki
## INSERTION BULK ET GEO_POINT (EX : COMMUNES FRANCAISES)
[ELK - 76. ELASTICSEARCH](https://www.youtube.com/watch?v=Zpszt2fbjFQ&list=PLn6POgpklwWrgJXXvbjlFPyHf8Q5a9n2b&index=77 "Xavki-ELK")  


## Mapping
>Création d'un index appelé communes  

```
vim index.json  

{
    "settings": {
        "number_of_shards": 3,
        "number_of_replicas": 2
    },
    "mappings": {
        "dynamic": "strict",
        "properties": {
            "departement_code": {"type": "text"},
            "ville": {"type": "text"},
            "code_postal": {"type": "text"},
            "location": {"type": "geo_point"}
        }
    }
}
```

```
curl -X PUT http://127.0.0.1:9200/communes -H "Content-Type: application/json" -d @index.json 
```

>Création du fichier d'injection des données  

```
cat cities.json | jq '.[] | .department_code + ";" + .name + ";" + .zip_code + ";" + (.gps_lng|tostring) + ";" + (.gps_lat|tostring) ' | tr -d '"' | awk -F ";" '{print "{ \"index\": {\"_index\":\"communes\"}} \n {\"departement_code\":\""$1"\",\"ville\":\""$2"\",\"code_postal\":\""$3"\",\"location\":["$4","$5"]}"}' > communes.json
```

>Mise en place des données  

```
curl -H "Content-Type: application/x-ndjson" -XPOST 127.0.0.1:9200/_bulk --data-binary @communes.json
```











