import requests

url = "http://localhost:9200/_bulk"
header = {"Content-Type": "application/x-ndjson"}

# Récupération du contenu du fichier json
data = open("dataset_cryptos_form.json", "rb")
data = data.read()

# On crée les data sur notre serveur elasticsearch
response = requests.post(url, headers=header, data=data)
print(response.status_code)
print(response.content)
