from flask import Flask
import requests

app = Flask(__name__)

@app.route('/')
def index():
    url = "http://localhost:9200/_bulk"
    header = {"Content-Type": "application/x-ndjson"}

    # Récupération du contenu du fichier json
    data = open("dataset_cryptos_form.json", "rb")
    data = data.read()

    # On crée les datas sur notre serveur elasticsearch
    response = requests.post(url, headers=header, data=data)
    print(response.status_code)
    print(response.content)

if __name__ == '__main__':
    app.run(debug=True)

# host=0.0.0.0, port=3000'''
