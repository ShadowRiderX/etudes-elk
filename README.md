## ELASTICSEARCH


### Commande de Base

>Renvoyer les informations générale d'elasticsearch : nom du cluster, version  

```
curl 127.0.0.1:9200  
```

>Recherche les index  

```
curl 127.0.0.1:9200/_cat/indices  
```

>Pour affiche les réponses de manière plus lisible rajouter ?pretty  
exemple :   
* curl 127.0.0.1:9200/utilisateurs/_search?=pretty  
* curl 127.0.0.1:9200/utilisateurs/_search?pretty  
avec "utilisateurs" comme index   

>Statistique et information  

* Sur tout les cluster  

```
curl 127.0.0.1:9200/_cluster/stats?pretty
```

* Sur un noeud en particulier  

```
curl 127.0.0.1:9200/_cluster/stats/nodes/nom_du_noeud?pretty
```

>Information sur les shards  

```
curl 127.0.0.1:9200/_cat/shards?pretty
```



### Mapping

[mapping-types](https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping-types.html, "mapping-types")  

>Création d'un index mappé => utilisateurs  

```
curl -X PUT http://127.0.0.1:9200/utilisateurs -H 'Content-Type: application/json' -d \
'{
    "mappings": {
        "dynamic": "strict",
        "properties": {
            "nom": {"type": "text"},
            "ville": {"type": "text"}
        }
    }
}'
```

>Ajout des données simple  

* Méthode 1

```
curl -XPOST -H 'Content-Type: Application/json' 127.0.0.1:9200/utilisateurs/_doc -d \
'{"nom": "alexis", "ville": "rennes"}'
```

Ajouter /1 par exemple après /_doc pour définir un id=1  
Sinon elasticsearch definira un id aléatoire automatiquement  

* Méthode 2
Crée un fichier : vim indiv
{"nom": "Pauline", "ville": "Lille"}

```
curl -XPOST -H 'Content-Type: application/x-ndjson' 127.0.0.1:9200/utilisateurs/_doc/_bulk --data-binary @indiv
```

>Ajouts de données multiples dans les index  

vim indiv.json
{ "index" : { "_index" : "utilisateurs", "_id" : "1" } }
{"nom": "Marc", "ville": "Rennes"}
{ "index" : { "_index" : "utilisateurs", "_id" : "2" } }
{"nom": "Pauline", "ville": "Paris"}

```
curl -s -H "Content-Type: application/x-ndjson" -XPOST 127.0.0.1:9200/_bulk --data-binary "@indiv.json"
```

>Mise à jours des index  

```
curl -XPOST -H 'Content-Type: Application/x-ndjson' 127.0.0.1:9200/idx3/_update/num_id -d \
'{ "doc" : {"nom": "Alice", "ville": "Paris"} }'
```

Attention :  
Elasticsearch ne concerve pas les anciennes données mais indique seulement le numéro de version.  
"_version" : numéro-de-version  



### Supprimer des données

>Supression de l'index utilisateurs  

```
curl  -XDELETE http://localhost:9200/utilisateurs
```

>Supprimer une donnée via une table  

```
curl  -XDELETE http://localhost:9200/utilisateurs/_doc
```

>Supprimer une donnée via son ID  

```
curl  -XDELETE http://localhost:9200/utilisateurs/_doc/id
```



### Recherche

#### Avec jq

```
curl 127.0.0.1:9200/exemple_index/_doc/exemple_id | jq '._source.exemple_qqch.exemple_qqch'
```

#### Recherche full de type match_all  

>Retourne tout les documents  

```
curl -XGET 127.0.0.1:9200/utilisateurs/_search?=pretty 
```

#### Recherche full avec filtres  

>Retourne tout les documents qui contient Alexis  

```
curl -XGET 127.0.0.1:9200/utilisateurs/_search?q=Alexis
```

#### Recherche full avec mapping  

>Retourne tout les documents qui contient Alexis  

```
curl -XGET -H 'Content-Type: application/json' 127.0.0.1:9200/utilisateurs/_search?pretty -d \
'{
    "query": {
        "match": {
            "nom": "Alexis"
        }
    }
}'
```

>Retourne tout les documents qui contient les nom Alice et Alexis  

```
curl -XGET -H 'Content-Type: application/json' 127.0.0.1:9200/utilisateurs/_search?pretty -d \
'{
    "query": {
        "match": {
            "nom": "Alice Alexis"
        }
    }
}'
```

>Recherches de phrase exacte  
Retourne tout les documents qui contient exactement le terme "Bob Alice"  

```
curl -XGET -H 'Content-Type: application/json' 127.0.0.1:9200/utilisateurs/_search?pretty -d \
'{
    "query": {
        "match_phrase": {
            "nom": "Bob Alice"
        }
    }
}'
```

#### Recherche multi champs

>On a cherche le term Alexis dans tous les champs de tous les documents  

```
curl -XGET -H 'Content-Type: application/json' 127.0.0.1:9200/utilisateurs/_search?q=Alexis
```

>Recherche des champs nom et ville  
Contenue de la recherche  
* "query": "Rennes"  
Champs à séléctionner  
* "fields": ["nom", "ville"]  

```
curl -XGET -H 'Content-Type: application/json' 127.0.0.1:9200/utilisateurs/_search?pretty -d \
'{
    "query": {
        "multi_match": {
            "query": "Rennes",
            "fields": ["nom", "ville"]
        }
    }
}'
```

>Recherche aproximative :: Fuzziness  
Permet de spécifier un degré de précision dans les recherches.  
exemple : Rnenes presque pareil que Rennes, il renveras alors Rennes MAIS avec un _score petit...  

```
curl -XGET -H 'Content-Type: application/json' 127.0.0.1:9200/utilisateurs/_search?pretty -d \
'{
    "query": {
        "multi_match": {
            "query": "Rnenes",
            "fields": ["nom", "ville"],
            "fuzziness": 2
        }
    }
}'
```

>Recherche par terms exactes  
Recherche le term nom qui à pour valeur Alexis  

```
curl -XGET -H 'Content-Type: application/json' 127.0.0.1:9200/utilisateurs/_search?pretty -d \
'{
    "query": {
        "term": {
            "nom": {
                "value": "Alexis"
            }
        }
    }
}'
```

>Recherche par terms approximatifs  
Recherche le term nom qui à pour valeur approché Aelxsi  

```
curl -XGET -H 'Content-Type: application/json' 127.0.0.1:9200/utilisateurs/_search?pretty -d \
'{
    "query": {
        "fuzzy": {
            "nom": {
                "value": "Aelxsi"
            }
        }
    }
}'
```











