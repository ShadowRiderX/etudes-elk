from flask import Flask, render_template, request
from elasticsearch import Elasticsearch

app = Flask(__name__)

es = Elasticsearch("127.0.0.1:9200")

@app.route('/', methods=["GET", "POST"])
def index():
    recherche = request.args.get("q")
    # recherche = request.form.get("q")
    if recherche is not None:
        response = es.search(index='utilisateurs',
                             body={"query": {
                                 "multi_match": {
                                     "query": recherche,
                                     "fields": ["nom", "ville"],
                                     "fuzziness": 2}
                             }})
        print("##########################")
        print(response)
        print(type(response))
        print(len(response))
        print("##########################")
        print("##########################")
        for element in response:
            print(element)
            print(type(element))
            print(len(element))
        print("##########################")
        print(response["hits"])
        print(type(response["hits"]))
        print(len(response["hits"]))
        print("##########################")
        for elementHits in response["hits"]["hits"]:
            print(elementHits)
            print(type(elementHits))
            print(len(elementHits))
        print("##########################")
        print(response["hits"]["hits"])
        print(type(response["hits"]["hits"]))
        print(len(response["hits"]["hits"]))
        print("##########################")
        for elementlisthits in response["hits"]["hits"]:
            print(elementlisthits["_source"]["nom"])
            print(elementlisthits["_source"]["ville"])
        print("##########################")
        return render_template("response.html", q=recherche, elementlisthits=elementlisthits, response=response)
    return render_template("index.html")

if __name__ == '__main__':
    app.run(debug=True)
